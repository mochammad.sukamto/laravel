@extends('adminlte.master')

@section('content')
    <div class="ml-3 mt-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Cast Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="card-footer">
                  <a class="btn btn-primary" href="/cast/create" >Create New Post</a>
                </div>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">No</th>
                      <th>Nama</th>
                      <th>Umur</th>
                      <th>Bio</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1.</td>
                      <td>Update software</td>
                      <td>1.</td>
                      <td>testing</td>
                      <td><span class="badge bg-danger">55%</span></td>
                    </tr>
                  </tbody>
                  @foreach ($cast as $key => $cast)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $cast->nama }}</td>
                        <td>{{ $cast->umur }}</td>
                        <td>{{ $cast->bio }}</td>
                        <td style="display: flex;">
                            <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm">show</a>
                            <a href="/cast/{{$cast->id}}/edit" class="btn btn-default btn-sm">edit</a>
                            <form action="/cast/{{$cast->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                  @endforeach
                </table>
              </div>
              <!-- /.card-body -->
            </div>
    </div>
@endsection