@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3" >
    <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Create New Cast</h3>
            </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/cast" method="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleNama">Nama</label>
                    <input type="text" name="nama" class="form-control" id="exampleInputEmail1" placeholder="Nama">
                  </div>
                  <div class="form-group">
                    <label for="exampleUmur">Umur</label>
                    <input type="text" name="umur" class="form-control" id="exampleInputEmail1" placeholder="Umur">
                  </div>
                  <div class="form-group">
                    <label for="exampleBio">Bio</label>
                    <input type="text" name="bio" class="form-control" id="exampleInputEmail1" placeholder="Bio">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
    </div>    
@endsection