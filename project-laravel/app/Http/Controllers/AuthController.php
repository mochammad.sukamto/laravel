<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function home(){
        return view('home');
    }

    public function home_post(Request $request){
        $namad = $request["first"];
        $namab = $request["last"];
        return view('home', compact("namad","namab"));
    }

}
