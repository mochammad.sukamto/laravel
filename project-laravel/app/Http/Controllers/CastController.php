<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }
    // public function create(Request $request){
    //     $nama = $request["nama"];
    //     $umur = $request["umur"];
    //     $bio = $request["bio"];
    //     return view('casts.create', compact("nama","umur","bio"));
    // }

    public function store(Request $request){
        // dd($request->all());

        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);

        return redirect('/cast/create');
    }

    public function index(){
        $cast = DB::table('cast')->get();
        // dd($cast->all());
        return view('cast.index', compact('cast'));
    }

    public function show($cast_id){
        $cast = DB::table('cast')->where('id', $cast_id)->first();
        // dd($cast);
        return view('cast.show', compact('cast'));
    }

    public function edit($cast_id){
        $cast = DB::table('cast')->where('id', $cast_id)->first();
        // dd($cast);
        return view('cast.edit', compact('cast'));
    }
    
    public function update($cast_id, Request $request){
        
        $query = DB::table('cast')
                ->where('id', $cast_id)
                ->update([
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']
                ]);
        return redirect('/cast')->with('success', 'Update Berhasil!');
    }

    public function destroy($cast_id){
        
        $query = DB::table('cast')->where('id', $cast_id)->delete();
        return redirect('/cast')->with('success', 'Hapus Berhasil!');
    }
}
